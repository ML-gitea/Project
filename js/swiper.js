
import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

const swiper = new Swiper('.swiper', {

  modules: [Navigation, Pagination],

  slidesPerView: 1,
  spaceBetween: 24,

  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
    clickable: true,
    
    formatFractionCurrent: function(number) {
      if (number < 10) {
      number = '0' + number;
      }
      return number;
    },

    formatFractionTotal: function(number) {
      if (number < 10) {
      number = '0' + number;
      }
      return number;
    },
  },

  breakpoints: {

    768: {
      slidesPerView: 'auto',
      spaceBetween: 24,

      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
      },

    },

  },

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
    
});